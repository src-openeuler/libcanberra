Name:             libcanberra
Version:          0.30
Release:          27
Summary:          XDG Sound Theme and Name Specifications
License:          LGPLv2+
Url:              https://0pointer.de/lennart/projects/libcanberra
Source0:          http://0pointer.de/lennart/projects/libcanberra/%{name}-%{version}.tar.xz

BuildRequires:    gcc gtk2-devel gtk3-devel alsa-lib-devel libvorbis-devel libtool-ltdl-devel gtk-doc
BuildRequires:    gstreamer1-devel libtdb-devel gettext-devel systemd-devel make
%if %{?openEuler:1}0
BuildRequires:    pulseaudio-libs-devel
Requires:         pulseaudio-libs
%endif
Requires:         sound-theme-freedesktop
%systemd_requires

%description
libcanberra is an implementation of the XDG Sound Theme and Name Specifications, for generating 
event sounds on free desktops, such as GNOME. It comes with several backends (ALSA, PulseAudio, OSS, 
GStreamer, null) and is designed to be portable

%package          gtk2
Summary:          Gtk+ 2.x Bindings for libcanberra
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-gtk3 = %{version}-%{release}

%description      gtk2
Gtk+ 2.x bindings for libcanberra

%package          gtk3
Summary:          Gtk+ 3.x Bindings for libcanberra
Requires:         %{name} = %{version}-%{release}

%description      gtk3
Gtk+ 3.x bindings for libcanberra

%package          devel
Summary:          Development Files and Header files for %{name}
Requires:         %{name} = %{version}-%{release} %{name}-gtk2 = %{version}-%{release}
Requires:         %{name}-gtk3 = %{version}-%{release} gtk2-devel
  
%description      devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure \
%if !0%{?openEuler}
    --disable-pulse \
%else
    --enable-pulse \
%endif
    --enable-alsa --enable-null --disable-oss --with-builtin=dso --with-systemdsystemunitdir=/usr/lib/systemd/system
%make_build

%install
%make_install
%delete_la_and_a
rm $RPM_BUILD_ROOT%{_docdir}/libcanberra/README

%post
/sbin/ldconfig
%systemd_post canberra-system-bootup.service canberra-system-shutdown.service canberra-system-shutdown-reboot.service

%preun
%systemd_preun canberra-system-bootup.service canberra-system-shutdown.service canberra-system-shutdown-reboot.service

%postun
/sbin/ldconfig
%systemd_postun canberra-system-bootup.service canberra-system-shutdown.service canberra-system-shutdown-reboot.service

%ldconfig_scriptlets gtk2
%ldconfig_scriptlets gtk3

%files
%defattr(-,root,root)
%doc LGPL
%license LGPL
%{_bindir}/canberra-boot
%{_libdir}/libcanberra.so.*
%{_libdir}/libcanberra-0.30/*.so
%{_prefix}/lib/systemd/system/*.service

%files gtk2
%defattr(-,root,root)
%{_libdir}/gtk-2.0/modules/libcanberra-gtk-module.so
%{_libdir}/libcanberra-gtk.so.*

%files gtk3
%defattr(-,root,root)
%{_bindir}/canberra-gtk-play
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/*.desktop
%{_libdir}/gtk-3.0/modules/*.so
%{_libdir}/libcanberra-gtk3.so.*
%{_datadir}/gdm/autostart/LoginWindow/*.desktop
%{_datadir}/gnome/*

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_datadir}/vala/vapi/*.vapi

%files help
%doc README
%{_datadir}/gtk-doc/html/*

%changelog
* Mon Nov 28 2022 wangkerong <wangkerong@h-partners.com> - 0.30-27
- add make buildrequire 

* Tue Mar 1 2022 hanhui <hanhui15@h-partners.com> - 0.30-26
- DESC: custom installation depend on pulseaudio-libs

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.30-25
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: optimization the spec

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-24
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the spec

* Mon Nov 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-23
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:modify the changelog

* Tue Oct 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-22
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the service for systemd_postun

* Mon Sep 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-21
- Package init
